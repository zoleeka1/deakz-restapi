package com.deakz.java.restapidemo.service.impl;

import com.deakz.java.restapidemo.domain.RandomCity;
import com.deakz.java.restapidemo.domain.User;
import com.deakz.java.restapidemo.repository.RandomCityRepository;
import com.deakz.java.restapidemo.repository.UserRepository;
import com.deakz.java.restapidemo.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by nydiarra on 07/05/17.
 */
@Service
public class GenericServiceImpl implements GenericService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RandomCityRepository randomCityRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Override
    public List<User> findAllUsers() {
        return (List<User>)userRepository.findAll();
    }

    @Override
    public List<RandomCity> findAllRandomCities() {
        return (List<RandomCity>)randomCityRepository.findAll();
    }
}
