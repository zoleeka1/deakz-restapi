package com.deakz.java.restapidemo.service;

import com.deakz.java.restapidemo.domain.RandomCity;
import com.deakz.java.restapidemo.domain.User;

import java.util.List;

/**
 * Created by nydiarra on 06/05/17.
 */
public interface GenericService {
    User findByUsername(String username);

    List<User> findAllUsers();

    List<RandomCity> findAllRandomCities();
}
